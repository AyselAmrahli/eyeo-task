window.addEventListener("load", function() {




    var tabs_links = document.querySelectorAll("ul.tab-nav a.tab-nav-link");
    

    function switchTab($event) {
		for (var i = 0; i < tabs_links.length; i++) {
			tabs_links[i].classList.remove("active");
		}
		var clickedTab = $event.currentTarget;
		clickedTab.classList.add("active");
		$event.preventDefault();
		var contentBodies = document.querySelectorAll(".tab-body");


		for (i = 0; i < contentBodies.length; i++) {
			contentBodies[i].classList.remove("active");
		}

		
		var anchorReference = $event.target;
		var activeBodyId = anchorReference.getAttribute("href");
		var activeBody = document.querySelector(activeBodyId);
        activeBody.classList.add("active");
        
        console.log(tabs_links)
	}

    for (i = 0; i < tabs_links.length; i++) {
		tabs_links[i].addEventListener("click", switchTab)
    }
});